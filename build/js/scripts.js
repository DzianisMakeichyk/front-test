// Issue status
const issues = document.querySelectorAll('.issue');
const tabsNav = document.querySelectorAll('.tabNav');

function handleIssue() {
    this.classList.toggle('issue--selected');
}

function handleTabNav() {
    tabsNav.forEach(function (tab) {
        tab.classList.remove('tabNav--selected');
    });
    this.classList.add('tabNav--selected');
}

issues.forEach(function (issue) {
    issue.addEventListener('click', handleIssue);
});

tabsNav.forEach(function (tab) {
    tab.addEventListener('click', handleTabNav);
});


//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJzY3JpcHRzLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8vIElzc3VlIHN0YXR1c1xuY29uc3QgaXNzdWVzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmlzc3VlJyk7XG5jb25zdCB0YWJzTmF2ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLnRhYk5hdicpO1xuXG5mdW5jdGlvbiBoYW5kbGVJc3N1ZSgpIHtcbiAgICB0aGlzLmNsYXNzTGlzdC50b2dnbGUoJ2lzc3VlLS1zZWxlY3RlZCcpO1xufVxuXG5mdW5jdGlvbiBoYW5kbGVUYWJOYXYoKSB7XG4gICAgdGFic05hdi5mb3JFYWNoKGZ1bmN0aW9uICh0YWIpIHtcbiAgICAgICAgdGFiLmNsYXNzTGlzdC5yZW1vdmUoJ3RhYk5hdi0tc2VsZWN0ZWQnKTtcbiAgICB9KTtcbiAgICB0aGlzLmNsYXNzTGlzdC5hZGQoJ3RhYk5hdi0tc2VsZWN0ZWQnKTtcbn1cblxuaXNzdWVzLmZvckVhY2goZnVuY3Rpb24gKGlzc3VlKSB7XG4gICAgaXNzdWUuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBoYW5kbGVJc3N1ZSk7XG59KTtcblxudGFic05hdi5mb3JFYWNoKGZ1bmN0aW9uICh0YWIpIHtcbiAgICB0YWIuYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCBoYW5kbGVUYWJOYXYpO1xufSk7XG5cbiJdLCJmaWxlIjoic2NyaXB0cy5qcyJ9
