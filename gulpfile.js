/**
 * Created by Dzianis Makeichyk on 20/10/2018.
 */
var gulp =                     require('gulp'),
    sourcemaps =               require('gulp-sourcemaps'),
    sass =                     require('gulp-sass'),
    autoprefixer =             require('gulp-autoprefixer'),
    browserSync =              require('browser-sync'),
    handlebars =               require('gulp-compile-handlebars'),
    rename =                   require('gulp-rename'),
    plumber =                  require('gulp-plumber'),
    postcssDiscardDuplicates = require('postcss-discard-duplicates'),
    postcssDiscardEmpty =      require('postcss-discard-empty'),
    postcssFlexbugsFixes =     require('postcss-flexbugs-fixes'),
    postcssRoundSubpixels =    require('postcss-round-subpixels'),
    htmlmin =                  require('gulp-htmlmin'),
    useref =                   require('gulp-useref'),
    minify =                   require('gulp-minify'),
    image =                    require('gulp-image');

/* description */
var hbsFiles =                 require('./app/dates/pages.json');
var issues =          require('./app/dates/issues.json');

/* pathConfig */
var browserDir =               './build',
    sassWatchPath =            './app/scss/**/*.scss',
    jsWatchPath =              './app/js/**/*.js',
    hbsWatchPath =             './app/hbs/**/*.hbs';
    htmlWatchPath =            './build/*.html';
/**/

/* hbs */
gulp.task('hbs', function () {
    templateData = issues,
        options = {
            ignorePartials: true,
            batch : [
                //If need add more direction here
                './app/hbs/informations',
                './app/hbs/components',
                './app/hbs/pages',
                './app/hbs/'
            ]
        };

    for (var i = 0; i < hbsFiles.length; i++) {
        gulp.src('./app/hbs/pages/' + hbsFiles[i] + '.hbs')
            .pipe(handlebars(templateData, options))
            .pipe(rename(hbsFiles[i] + '.html'))
            .pipe(gulp.dest('./build'));
    }
});
/**/

/* JS ES5 */
gulp.task('js', function () {
    return gulp.src(jsWatchPath)
        .pipe(sourcemaps.init())
        .pipe(useref())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./build/js/'))
});
/**/

/* Scss */
gulp.task('scss', function () {
    return gulp.src('./app/scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 3 versions']
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./build/css'))
        .pipe(browserSync.reload({stream: true}));
});
/**/

/* browserSync */
gulp.task('browser-sync', function () {
    browserSync.init({
        "server": {
            "baseDir": browserDir
        }
    });
});
/**/

/* Watch */
gulp.task('watch', function () {
    gulp.watch(jsWatchPath, ['js']);
    gulp.watch(sassWatchPath, ['scss']);
    gulp.watch(hbsWatchPath, ['hbs']);
    gulp.watch(htmlWatchPath, function () {
        return gulp.src('')
            .pipe(browserSync.reload({stream: true}))
    });
});
/**/

/* Run project (ES5)*/
gulp.task('run', ['js', 'hbs', 'scss', 'watch', 'browser-sync']);
/**/

// Production Styles w/o lint, source maps & with compression to optimize speed
gulp.task('hbs-prod', function() {
    return gulp.src(htmlWatchPath)
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true,
            removeCommentsFromCDATA: true,
            minifyJS: true,
            minifyCSS: true,
            ignoreCustomFragments: [/{{[\s\S]*?}}/]
        }))
        .pipe(gulp.dest('./dist'));
});

gulp.task('scss-prod', function () {
    return gulp.src(sassWatchPath)
        .pipe(plumber())
        .pipe(sass({
            errLogToConsole: true,
            outputStyle: 'compressed'
        }))
        .pipe(autoprefixer({
                browsers: ['last 3 versions']
            }),
            postcssDiscardDuplicates,
            postcssDiscardEmpty,
            postcssRoundSubpixels,
            postcssFlexbugsFixes
        )
        .pipe(plumber.stop())
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('js-prod', function () {
    return gulp.src(jsWatchPath)
        .pipe(sourcemaps.init())
        .pipe(useref())
        .pipe(sourcemaps.write())
        .pipe(minify())
        .pipe(gulp.dest('./dist/js/'))
});

gulp.task('img-prod', function() {
    return gulp.src('build/img/**/*.+(png|jpg|jpeg|gif|svg)')
        .pipe(image({
            pngquant: true,
            optipng: false,
            zopflipng: true,
            jpegRecompress: false,
            mozjpeg: true,
            guetzli: false,
            gifsicle: true,
            svgo: true,
            concurrent: 10
        }))
        .pipe(gulp.dest('dist/img'));
});

/* Build project */
gulp.task('build', ['hbs-prod', 'scss-prod', 'js-prod', 'img-prod']);
/**/
