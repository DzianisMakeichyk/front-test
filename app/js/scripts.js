// Issue status
const issues = document.querySelectorAll('.issue');
const tabsNav = document.querySelectorAll('.tabNav');

function handleIssue() {
    this.classList.toggle('issue--selected');
}

function handleTabNav() {
    tabsNav.forEach(function (tab) {
        tab.classList.remove('tabNav--selected');
    });
    this.classList.add('tabNav--selected');
}

issues.forEach(function (issue) {
    issue.addEventListener('click', handleIssue);
});

tabsNav.forEach(function (tab) {
    tab.addEventListener('click', handleTabNav);
});

